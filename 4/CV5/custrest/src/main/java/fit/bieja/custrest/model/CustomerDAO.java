package fit.bieja.custrest.model;

import fit.bieja.custrest.business.LoggerBinder;
import org.jboss.resteasy.api.validation.Validation;

import javax.annotation.PostConstruct;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

@ApplicationScoped
public class CustomerDAO {

    @Inject
    Validator validator;

    private NavigableMap<Integer, Customer> customers = new TreeMap<Integer, Customer>();

    public List<Customer> all ()
    {
        return new ArrayList(customers.values());
    }

   // @LoggerBinder
    public int create(String name) {

        Customer c = new Customer(customers.lastKey() + 1, name);
        Set<ConstraintViolation<Customer>> vc = validator.validate(c);
        if(vc.isEmpty())
        {
            customers.put(c.getId(), c);
            return c.getId();
        } else
        {
            throw new InvalidCustomer(vc.toString());
        }


    }

    @PostConstruct
    void init() {
        customers.put(1, new Customer(1, "Tom"));
    }

    public Customer get(int id) {
        return customers.get(id);
    }

    public void update(int id, String newName) {
        customers.get(id).setName(newName);
    }



}
