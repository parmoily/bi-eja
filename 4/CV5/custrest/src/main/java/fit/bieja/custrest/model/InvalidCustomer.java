package fit.bieja.custrest.model;


public class InvalidCustomer extends RuntimeException{
    public InvalidCustomer() {
    }

    public InvalidCustomer(String message) {
        super(message);
    }
}
