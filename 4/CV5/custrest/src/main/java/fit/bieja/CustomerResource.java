package fit.bieja;

import fit.bieja.custrest.model.Customer;
import fit.bieja.custrest.model.CustomerDAO;
import fit.bieja.custrest.resources.CustomerSubResources;

import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

@Path("/customers")
public class CustomerResource{
    @Inject
    CustomerDAO custDAO;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> customers() {
        return custDAO.all();
    }


    @POST
    public Response create(String name, @Context UriInfo info)
    {
        int id = custDAO.create(name);
        URI location = UriBuilder.fromUri(info.getBaseUri()).build(id);
        return Response.created(location).build();
    }



    @Path("{id}")
    public CustomerSubResources customer(@PathParam("id") int id) {
        return new CustomerSubResources(id);
    }


    @PUT
    public Response createPut(@Context UriInfo info)
    {
        int id = custDAO.create("INVALID");

        CustomerSubResources tmp = new CustomerSubResources(id);
        tmp.update("Valid");

        URI location = UriBuilder.fromUri(info.getBaseUri()).build(id);
        return Response.created(location).build();
    }
}