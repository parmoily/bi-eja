package fit.bieja.custrest.resources;

import fit.bieja.custrest.business.AuthorizationError;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AuthorizationErrorMapper implements ExceptionMapper<AuthorizationError> {

    @Override
    public Response toResponse(AuthorizationError authorizationError) {
        return Response.status(Response.Status.FORBIDDEN).build();
    }
}
