package fit.bieja.custrest.resources;

import fit.bieja.custrest.model.InvalidCustomer;

import javax.swing.text.html.parser.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidCustomerMapper implements ExceptionMapper<InvalidCustomer> {

    @Override
    public Response toResponse(InvalidCustomer invalidCustomer) {
        return Response.status(Response.Status.BAD_REQUEST).entity(invalidCustomer.getMessage()).build();
    }
}
