package fit.bieja.custrest.resources;


import fit.bieja.custrest.business.LoginBean;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("login")
public class LoginResource {

    private LoginBean lb;

    @Inject
    public LoginResource(LoginBean lb) {
        this.lb = lb;
    }

    @PUT
    public Response login(String user) {
        if (lb.login(user)) {
            return Response.accepted().build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    public void logout() {
        lb.logout();
    }


}
