package fit.bieja.custrest.resources;

import fit.bieja.custrest.business.LoginBean;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerRequestFilter ;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Logger;


@Provider
public class ServerFilter implements ContainerRequestFilter  {
    @Inject
    LoginBean loginBean;

    private static final Logger LOG = Logger.getLogger(ServerFilter.class.getName());


    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        Optional<String> user = Optional.ofNullable(crc.getHeaderString("user"));
        user.ifPresent(u -> {
            loginBean.login(u);
        });
        LOG.info(() -> String.format("method: %s,  cookies: %s\n", crc.getMethod(), crc.getCookies()));
    }

}
