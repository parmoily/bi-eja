package fit.bieja.custrest.business;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

@Interceptor
@LoggerBinder
public class LoginInterceptor {
    @Inject
    LoginBean lb;

    @AroundInvoke
    Object isAllowed(InvocationContext ic) throws Exception {
        if (lb.isAdmin()) {
            return ic.proceed();
        } else {
            throw new AuthorizationError();
        }

    }


}
