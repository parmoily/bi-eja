package fit.bieja.custrest.model;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE,
        TYPE_USE })
@Retention(RUNTIME)
@Constraint(validatedBy = BannedCustomerValidator.class)
@Documented  //@Repeatable(List.class)
public @interface BannedCustomerConstraint {
    String message() default "Customer name is banned";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
