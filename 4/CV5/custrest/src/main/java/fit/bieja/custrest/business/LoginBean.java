package fit.bieja.custrest.business;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import java.util.Optional;
import java.util.Set;

@SessionScoped
public class LoginBean {
    public static final Set<String> admins = Set.of("adm");
    public static final Set<String> registered = Set.of("u1", "u2", "u3", "u4");

    private Optional<String> optUser;


    @PostConstruct
    void init() {
        optUser = Optional.empty();
    }


    public Optional<String> getUser() {
        return optUser;
    }

    public boolean isAdmin() {
        return optUser.isPresent()
                && admins.contains(optUser.get());
    }

    public boolean login(String user) {
        if (admins.contains(user) || registered.contains(user)) {
            optUser = Optional.of(user);

        }
        return optUser.isPresent();
    }

    @DELETE
    public void logout() {
        optUser = Optional.empty();
    }


}
