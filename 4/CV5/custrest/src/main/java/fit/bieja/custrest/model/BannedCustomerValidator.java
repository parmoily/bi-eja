package fit.bieja.custrest.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BannedCustomerValidator implements ConstraintValidator<BannedCustomerConstraint, String>  {
    private BannedCustomerConstraint bannedCustomerConstraint;

    @Override
    public void initialize(BannedCustomerConstraint constraintAnnotation) {
        bannedCustomerConstraint = constraintAnnotation;
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !s.equals("Hitler");
    }
}
