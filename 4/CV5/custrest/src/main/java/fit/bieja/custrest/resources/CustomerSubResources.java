package fit.bieja.custrest.resources;

import fit.bieja.custrest.model.Customer;
import fit.bieja.custrest.model.CustomerDAO;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;

//@Path("/customers2")
public class CustomerSubResources {
    @Inject
    CustomerDAO custDAO;
    int id;

    public CustomerSubResources(int id) {
        this.id = id;
    }

    public CustomerSubResources() {
        id = -1;
    }


    @GET
    public Customer get() {
        return custDAO.get(id);
    }

    @PUT
    public void update(String newName) {
        custDAO.update(id, newName);
    }



}
