package fit.bieja.restclient;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringReader;
import java.net.URI;
import java.util.List;

public class CustomerProxy {

    public static final URI CUSTOMERS = URI.create("http://localhost:8080/customers");
    public static final URI LOGIN = URI.create("http://localhost:8080/login");
    public static final CustomerProxy inst = new CustomerProxy();

    final Client restClient;
    private final WebTarget customers;
    private final WebTarget loginTarget;


    public CustomerProxy() {
        restClient = ClientBuilder.newClient();
        restClient.register(new ClientLogFilter());
        customers = restClient.target(CUSTOMERS);
        loginTarget = restClient.target(LOGIN);
    }

    public List<Customer> all()
    {
        return customers.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Customer>>(){});
    }

    public URI create(String name) {
        Response r = customers.request(MediaType.APPLICATION_JSON).post(Entity.text(name));

        if(r.getStatusInfo() == Response.Status.CREATED) {
            return r.getLocation();
        }
        throw new RuntimeException(r.readEntity(String.class));


//        String errorEntity = r.readEntity(String.class);
//        JsonObject o = Json.createReader(new StringReader(errorEntity)).readObject();
//        JsonObject jsonError = o.getJsonArray("parameterViolations").getJsonObject(0);
//        throw new RuntimeException(String.format("%s %s", jsonError.getString("path"), jsonError.getString("message")));
    }

    public URI createPut() {
        Response r = customers.request(MediaType.APPLICATION_JSON).put(Entity.text(""));
        return r.getLocation();
    }

    public void login(String user)
    {
        loginTarget.request().put(Entity.text(user));
    }

    public void logout()
    {
        loginTarget.request().delete();
    }
}
