package fit.bieja.restclient;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import java.io.IOException;

public class ClientLogFilter implements ClientResponseFilter {

    @Override
    public void filter(ClientRequestContext clientRequestContext, ClientResponseContext clientResponseContext) throws IOException {
        System.out.printf("request: %s: %s,  cookies: %s, headers: %s\n", clientRequestContext.getUri(), clientRequestContext.getMethod(), clientRequestContext.getCookies(), clientRequestContext.getHeaders());

        System.out.printf("response: %s: headers: %s\n", clientResponseContext.getStatusInfo(), clientResponseContext.getHeaders());
    }
}
