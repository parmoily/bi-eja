package fit.bieja.restclient;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

public class CustomerProxy {

    public static final CustomerProxy inst = new CustomerProxy();

    Client restClient = ClientBuilder.newClient();
    WebTarget customers = restClient.target("http://localhost:8080/customers");


    public CustomerProxy() {
    }

    public List<Customer> all()
    {
        return customers.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Customer>>(){});
    }

    public URI create(String name) {
        Response r = customers.request(MediaType.APPLICATION_JSON).post(Entity.text(name));
        return r.getLocation();
    }

    public URI createPut() {
        Response r = customers.request(MediaType.APPLICATION_JSON).put(Entity.text(""));
        return r.getLocation();
    }
}
