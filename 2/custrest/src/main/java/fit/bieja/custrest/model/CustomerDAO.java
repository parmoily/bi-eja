package fit.bieja.custrest.model;

import java.util.*;

public class CustomerDAO {

    public static final CustomerDAO inst = new CustomerDAO();

    private NavigableMap<Integer, Customer> customers = new TreeMap<Integer, Customer>();

    public CustomerDAO() {
        customers.put(1, new Customer(1, "Tom"));
    }

    public List<Customer> all ()
    {
        return new ArrayList(customers.values());
    }

    public int create(String name) {

        Customer c = new Customer(customers.lastKey() + 1, name);
        customers.put(c.getId(), c);

        return c.getId();
    }

    public void updateCustomer(int id, String newName)
    {

        for(Map.Entry<Integer, Customer> entry : customers.entrySet()) {

            if(entry.getValue().getId() == id)
            {
                customers.put(entry.getKey(), new Customer(id, newName));
            }
        }

    }


}
