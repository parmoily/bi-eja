package fit.bieja;

import fit.bieja.custrest.model.Customer;
import fit.bieja.custrest.model.CustomerDAO;
import fit.bieja.custrest.resources.CustomerSubResources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

@Path("/customers")
public class CustomerResource{

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> customers() {
        return CustomerDAO.inst.all();
    }


    @POST
    public Response create(String name, @Context UriInfo info)
    {
        int id = CustomerDAO.inst.create(name);
        URI location = UriBuilder.fromUri(info.getBaseUri()).build(id);
        return Response.created(location).build();
    }

    @PUT
    public Response createPut(@Context UriInfo info)
    {
        int id = CustomerDAO.inst.create("INVALID");

        CustomerSubResources tmp = new CustomerSubResources(id);
        tmp.update();

        URI location = UriBuilder.fromUri(info.getBaseUri()).build(id);
        return Response.created(location).build();
    }
}